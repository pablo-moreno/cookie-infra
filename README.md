# Cookie Infra

This repository is a template for creating development, staging and production infrastructures for Django/Vue projects in a single command.

## The command:

```bash
git@gitlab.com:pablo-moreno/cookie-infra.git
```


**project_slug:** Project folder name.

**dev_domain:** Development environment domain.

**domain:** Production environment domain.

**backend_git_repo_url:** Django project remote git url.

**frontend_git_repo_url:** Vue/Nuxt project remote git url.
