# Infrastructure

**Clone recursively this repo:**

**Install npm requirements:**

```bash
docker-compose run frontend npm i
```

**Start docker-compose:**

```bash
docker-compose up
```

**Edit /etc/hosts (or C:\Windows\System32\drivers\etc\hosts) file and add the following:**

```bash
127.0.0.1 localhost {{ cookiecutter.dev_domain }}
```

**Now you can access https://{{ cookiecutter.dev_domain }}**

The browser will show you a security alert because of the self-signed certificate, but just add an exception, and there you go.

**Happy coding :)**
