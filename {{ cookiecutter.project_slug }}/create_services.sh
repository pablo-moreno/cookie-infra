#!/bin/bash

# Create private network
docker network create -d overlay backend

# Create volumes mapped to the specified directories
docker volume create backend_db -d local-persist -o mountpoint=/var/www/backend/db
docker volume create backend_static -d local-persist -o mountpoint=/var/www/backend/static
docker volume create backend_media -d local-persist -o mountpoint=/var/www/backend/media

# POSTGRES service
docker service create \
  --name backend_postgres \
  --replicas 1 \
  --env "POSTGRES_USER=${PROJECT_DB_USER}" \
  --env "POSTGRES_PASSWORD=${PROJECT_DB_PASSWORD}" \
  --env "POSTGRES_DB=backend_backend" \
  --mount src=backend_db,dst=/var/lib/postgresql/data \
  --network backend \
  postgres:latest


# REDIS service
docker service create \
  --name backend_redis \
  --replicas 1 \
  --mount src=backend_db,dst=/var/lib/postgresql/data \
  --network backend \
  redis:latest


# BACKEND service
docker service create \
  --name backend_backend \
  --replicas 3 \
  --env "DATABASE_URL=postgres://$PROJECT_DB_USER:$PROJECT_DB_PASSWORD@backend_postgres:5432/backend_backend" \
  --publish "8000:8000" \
  --network backend \
  --mount src=backend_media,dst=/app/media \
  --mount src=backend_static,dst=/app/static \
  --entrypoint "./runserver.sh" \
  --with-registry-auth \
  django:latest
