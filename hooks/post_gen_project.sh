#!/bin/bash

BACKEND_REPO_URL="{{ cookiecutter.backend_git_repo_url }}"
FRONTEND_REPO_URL="{{ cookiecutter.frontend_git_repo_url }}"

FRONTEND_FRAMEWORK="{{ cookiecutter.frontend_framework }}"
EXISTING_REPOS="{{ cookiecutter.existing_repos }}"

git init

openssl req -nodes -new -x509  -keyout nginx/certs/server.key -out nginx/certs/server.crt

if [ "$EXISTING_REPOS" == "true" ]; then
  git submodule add -b master $BACKEND_REPO_URL backend/
  git submodule add -b master $FRONTEND_REPO_URL frontend/
  git submodule update --remote
else
  echo "Creating a new Django project. Set project_slug to backend. "
  pip3 install cookiecutter
  cookiecutter git@gitlab.com:pablo-moreno/cookie_django.git
  cd backend/ && git init && cd ../

  if [ "$FRONTEND_FRAMEWORK" == "vue" ]; then
    npm i -g @vue/cli
    vue create frontend
  else
    npx create-nuxt-app frontend
  fi
fi

touch .env.django
touch .env.frontend
